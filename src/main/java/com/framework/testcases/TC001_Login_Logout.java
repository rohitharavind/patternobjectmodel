package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_Login_Logout extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_Login_Logout";
		testDescription = "Login to LeafTaps";
		testNodes = "Leads";
		author = "Aravind";
		category= "Smoke";
		dataSheetName= "TC001";
}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickLogout();
		
		
	}
}
 