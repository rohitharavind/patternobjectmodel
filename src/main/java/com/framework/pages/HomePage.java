package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT,using="CRM/SFA") WebElement eleCRMSFA;
	@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogout;
	public MyHomePage clickCrmSfa() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCRMSFA);
		return new MyHomePage(); 
	}
	public LoginPage clickLogout() {
		//WebElement eleUsername = locateElement("id", "username");
		click(eleLogout);
		return new LoginPage(); 
	}


}
















