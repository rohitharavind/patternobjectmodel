package com.framework.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@name='id']") WebElement eleLeadId;
	@FindBy(how = How.LINK_TEXT, using = "Find Leads") WebElement eleLeadsButton;
	

	public FindLeadsPage enterLeadId(String Id) {

		clearAndType(eleLeadId, Id);
		return this;
	}

	public FindLeadsPage clickFindButton() {

		click(eleLeadsButton);
		return this;
	}
	
	@FindBy(how= How.CLASS_NAME, using = "x-grid3-row-table") WebElement table;
	@FindBy(how = How.LINK_TEXT, using = "firstMatchingID") WebElement eleMatchingId;
	List<WebElement> row = table.findElements(By.tagName("tr"));
	WebElement firstRow = row.get(0);
	List<WebElement> column = firstRow.findElements(By.tagName("td"));
	String firstMatchingID = column.get(0).getText();

	public DuplicateLeadPage clickFirstID() {

		click(eleMatchingId);
		return new DuplicateLeadPage();
	} 

}
