package com.framework.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {

	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}

//	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	//@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.CLASS_NAME,using="smallSubmit") WebElement eleCreateButton;

	public DuplicateLeadPage enterFirstName(String fName) {
		clearAndType(eleFirstName, fName);	
		return this; 
	}

	public ViewLeadPage clickCreateLeadButton() {
		click(eleCreateButton);
		return new ViewLeadPage(); 
	}
	
	
	
	

}
