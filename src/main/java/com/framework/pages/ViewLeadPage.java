package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement eleVfirstName;
	@FindBy(how = How.LINK_TEXT,using=" Duplicate Lead") WebElement eleDuplicate;
	public ViewLeadPage verifyFirstName() {
		verifyExactText(eleVfirstName,"Aravind");	
		return this; 
	}
	
	public ViewLeadPage clickDuplicateLead() {
		click(eleDuplicate);
		return this; 
	}


}
















